import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'login_screen.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  bool _hiden = false;
  bool isLoading = false;
  bool _hidenConfirmPassword = false;
  late String userRegister;

  final usernameController = TextEditingController();
  final emailController = TextEditingController();
  final phoneController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: Colors.white,
        body: CustomScrollView(
          slivers: [
            SliverAppBar(
              centerTitle: true,
              pinned: true,
              leading: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: const Icon(
                    CupertinoIcons.back,
                    color: Colors.black,
                  )),
              backgroundColor: Colors.white,
              title: const Text(
                "Create Account",
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
              ),
            ),
            SliverToBoxAdapter(
              child: SafeArea(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const Text(
                          "Enter your Name, Email , and Password for sign up. Already have account ?",
                          style: TextStyle(fontSize: 20.0),
                        ),
                        const SizedBox(height: 40.0),
                        TextField(
                          controller: usernameController,
                          style: const TextStyle(fontSize: 16.0),
                          decoration: const InputDecoration(
                            suffixIcon: Icon(Icons.person),
                            hintText: 'Enter Your Username',
                            labelText: "Username",
                            border: OutlineInputBorder(),
                          ),
                        ),
                        const SizedBox(height: 14.0),
                        TextField(
                          controller: emailController,
                          style: const TextStyle(fontSize: 16.0),
                          decoration: const InputDecoration(
                            suffixIcon: Icon(Icons.email),
                            hintText: 'Enter Your Email',
                            labelText: "Email",
                            border: OutlineInputBorder(),
                          ),
                        ),
                        const SizedBox(height: 14.0),
                        TextField(
                          controller: phoneController,
                          style: const TextStyle(fontSize: 16.0),
                          decoration: const InputDecoration(
                            suffixIcon: Icon(Icons.email),
                            hintText: 'Enter Your phone',
                            labelText: "phone",
                            border: OutlineInputBorder(),
                          ),
                        ),
                        const SizedBox(height: 14.0),
                        TextField(
                          controller: passwordController,
                          obscureText: _hiden,
                          style: const TextStyle(fontSize: 16.0),
                          decoration: InputDecoration(
                            hintText: 'Enter Your Password',
                            labelText: "Password",
                            border: const OutlineInputBorder(),
                            suffixIcon: IconButton(
                              onPressed: () {
                                if (_hiden) {
                                  setState(() => _hiden = false);
                                } else {
                                  setState(() => _hiden = true);
                                }
                              },
                              icon: Icon(
                                _hiden
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 14.0),
                        TextField(
                          controller: confirmPasswordController,
                          obscureText: _hiden,
                          style: const TextStyle(fontSize: 16.0),
                          decoration: InputDecoration(
                            hintText: 'Enter Your Confirm Password',
                            labelText: "Confirm Password",
                            border: const OutlineInputBorder(),
                            suffixIcon: IconButton(
                              onPressed: () {
                                if (_hiden) {
                                  setState(() => _hidenConfirmPassword = false);
                                } else {
                                  setState(() => _hidenConfirmPassword = true);
                                }
                              },
                              icon: Icon(
                                _hidenConfirmPassword
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 14.0),
                        SizedBox(
                          height: 50.0,
                          width: double.infinity,
                          child: CupertinoButton(
                            color: Color(0xFF263171),
                            child: const Text('SING UP'),
                            onPressed: () {
                              // register();
                            },
                          ),
                        ),
                        const SizedBox(height: 5.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Text('By Signing up you agree out Terms'),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Text('Conditions & Privacy Policy'),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
