import 'package:ecommerce_app/screens/dashboard/category_screen.dart';
import 'package:ecommerce_app/screens/dashboard/product_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  bool isLoaded = false;

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(milliseconds: 3000), () {
      setState(() {
        isLoaded = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.transparent,
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: true,
            leading: InkWell(
                onTap: () {
                  // Navigator.pop(context);
                },
                child: const Icon(
                  Icons.sort_sharp,
                  color: Colors.black,
                )),
            backgroundColor: Colors.white,
            title: const Text(
              "Welcome",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            ),
            actions: <Widget>[
              Container(
                child: IconButton(
                    icon: const Icon(
                      Icons.search,
                      color: Colors.black,
                    ),
                    onPressed: () {}),
              ),
              Container(
                child: IconButton(
                    icon: const Icon(
                      CupertinoIcons.cart,
                      color: Colors.black,
                    ),
                    onPressed: () {}),
              ),
              Container(
                child: IconButton(
                    icon: const Icon(
                      Icons.notifications_active_sharp,
                      color: Colors.black,
                    ),
                    onPressed: () {}),
              ),
            ],
          ),
          SliverToBoxAdapter(
            child: SizedBox(
                width: double.infinity,
                child: CarouselSlider.builder(
                    itemCount: 5,
                    options: CarouselOptions(
                      autoPlay: true,
                      aspectRatio: 16 / 9,
                      viewportFraction: 0.9,
                      initialPage: 0,
                      enlargeCenterPage: true,
                    ),
                    itemBuilder: (context, index, idx) {
                      return Container(
                        margin: const EdgeInsets.only(top: 10.0),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Image.network(
                              "https://images.pexels.com/photos/1382731/pexels-photo-1382731.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
                              fit: BoxFit.fill,
                              width: double.infinity,
                            )),
                      );
                    })),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding:
              const EdgeInsets.only(left: 20.0, top: 10.0, right: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text('Category'),
                  Text('view all'),
                ],
              ),
            ),
          ),
          SliverGrid(
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                return Container(
                  child: CategoryList(),
                );
              },
              childCount: 8,
            ),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 4,
              mainAxisSpacing: 0.5,
              crossAxisSpacing: 0.5,
              childAspectRatio: 1,
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding:
                  const EdgeInsets.only(left: 20.0, top: 10.0, right: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text('Product'),
                  Text('view all'),
                ],
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return Container(
                  child: ProductList(),
                );
              },
              childCount: 5,
            ),
          ),
        ],
      ),
    );
  }
}
