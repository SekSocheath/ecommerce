import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class ProductList extends StatelessWidget {
  const ProductList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          children: [
            ClipRRect(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(5),
                  topRight: Radius.circular(5),
                ),
                child: Image.network(
                  "https://images.pexels.com/photos/1308885/pexels-photo-1308885.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
                  fit: BoxFit.fill,
                  height: 150,
                  width: double.infinity,
                )),
            Padding(
              padding: const EdgeInsets.only(left: 10.0,top: 5.0),
              child: Container(
                child: Row(
                  children: [
                    Container(child: Text('Name')),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0,right: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(child: Text('Price')),
                  Container(child: Text('descounts'))
                ],
              ),
            ),
            SizedBox(height: 5,)
          ],
        ),
      ),
    );
  }
}
