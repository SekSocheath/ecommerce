import 'package:flutter/material.dart';

class CategoryList extends StatelessWidget {
  const CategoryList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
      ),
      child: Card(
            elevation: 3,
            child: Column(
              children: [
                ClipRRect(
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5),
                    ),
                    child: Image.network(
                      "https://www.remove.bg/example.jpg",
                      fit: BoxFit.fill,
                      width: double.infinity,
                    )),
                Text('name'),
              ],
            ),
      ),
    );
  }
}
