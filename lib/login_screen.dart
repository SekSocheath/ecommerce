import 'package:ecommerce_app/screens/dashboard/dashboard_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'register_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _hiden = true;
  bool isLoading = false;

  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  // void login() {
  //   setState(() {
  //     isLoading = true;
  //   });
  //   var email = _emailController.text;
  //   var password = _passwordController.text;
  //   if (email == "" && password == "") {
  //     setState(() {
  //       isLoading = false;
  //     });
  //     showDialog(
  //       context: context,
  //       builder: (context) => AlertDialog(
  //         title: Text("Error"),
  //         content: Text("Please Enter email and password"),
  //       ),
  //     );
  //   } else {
  //     LoginReq req = new LoginReq();
  //     req.email = email.trim().toString();
  //     req.password = password.trim().toString();
  //     String userRole;
  //     String adminRole;
  //     ApiManager()
  //         .login(req)
  //         .then((value) => {
  //       setState(() {
  //         isLoading = false;
  //       }),
  //       userRole = "",
  //       adminRole = "",
  //       value.roles!.forEach((element) {
  //         if (element == "ROLE_ADMIN") {
  //           adminRole = "ROLE_ADMIN";
  //         }
  //         if (element == "ROLE_USER") {
  //           userRole = "ROLE_USER";
  //         }
  //       }),
  //       saveUserToLocalStorage(User(
  //           id: value.id,
  //           username: value.username,
  //           email: value.email,
  //           accessToken: value.accessToken,
  //           refreshToken: value.refreshToken,
  //           adminRole: adminRole,
  //           userRole: userRole)),
  //     })
  //         .onError((error, stackTrace) => {
  //       setState(() {
  //         isLoading = false;
  //       }),
  //       showDialog(
  //         context: context,
  //         builder: (context) => AlertDialog(
  //           title: Text("Error"),
  //           content: Text("Can not login"),
  //         ),
  //       ),
  //     });
  //   }
  // }
  //
  // void saveUserToLocalStorage(User user) {
  //   UserSharedPreference().saveUser(user);
  //   UserSharedPreference().saveDemoScreen();
  //   Navigator.pushReplacement(
  //     context,
  //     MaterialPageRoute(builder: (context) => const App()),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: SafeArea(
                child: isLoading
                    ? Center(child: CircularProgressIndicator())
                    : Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const Padding(padding: EdgeInsets.only(top: 30.0)),
                        const Text(
                          "Welcome",
                          style: TextStyle(
                              fontSize: 30.0, fontWeight: FontWeight.bold),
                        ),
                        const Padding(padding: EdgeInsets.only(top: 20.0)),
                        const Text(
                          "Enter your Email address to sign in.",
                          style: TextStyle(fontSize: 20.0),
                        ),
                        const SizedBox(height: 10.0),
                        const Text(
                          "Enjoy your stay.",
                          style: TextStyle(fontSize: 20.0),
                        ),
                        const SizedBox(height: 50.0),
                        Form(
                          child: Column(
                            children: [
                              TextField(
                                controller: _emailController,
                                style: const TextStyle(fontSize: 16.0),
                                decoration: const InputDecoration(
                                  suffixIcon: Icon(Icons.email),
                                  hintText: 'Enter Your Email',
                                  labelText: "Email",
                                  border: OutlineInputBorder(),
                                ),
                              ),
                              const SizedBox(height: 14.0),
                              TextField(
                                controller: _passwordController,
                                obscureText: _hiden,
                                style: const TextStyle(fontSize: 16.0),
                                decoration: InputDecoration(
                                  hintText: 'Enter Your Password',
                                  labelText: "Password",
                                  border: const OutlineInputBorder(),
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      if (_hiden) {
                                        setState(() => _hiden = false);
                                      } else {
                                        setState(() => _hiden = true);
                                      }
                                    },
                                    icon: Icon(
                                      _hiden
                                          ? Icons.visibility_off
                                          : Icons.visibility,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        //const SizedBox(height: 2.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            TextButton(
                              onPressed: () {},
                              child: const Text('Forgot Password ?'),
                            )
                          ],
                        ),
                        const SizedBox(height: 6.0),
                        SizedBox(
                          height: 50.0,
                          width: double.infinity,
                          child: CupertinoButton(
                            color: Color(0xFF263171),
                            child: const Text('SING IN'),
                            onPressed: () {
                              //Navigator.pop(context);
                              // this.login();
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                  const DashboardScreen(),
                                ),
                              );
                            },
                          ),
                        ),
                        const SizedBox(height: 5.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text('Don\'t has an account?'),
                            TextButton(
                              onPressed: () {
                                // Navigator.pop(context);
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) =>
                                    const RegisterScreen(),
                                  ),
                                );
                              },
                              child: const Text('Create new account.'),
                            )
                          ],
                        ),
                        const SizedBox(height: 5.0),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
